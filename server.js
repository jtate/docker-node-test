var express = require('express');

var port = 8000;

var app = express();
app.get('/', function (req, res) {
	res.status(200).send('welcome to jody\'s test of docker');
});

app.listen(port);
console.log('docker node test on http://localhost:%s', port);
